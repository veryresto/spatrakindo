package id.co.trakindo.todo;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = "/list-todos")
public class ListTodosServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	TodoService service;
	String inputString = "Hello World!";
	byte[] b = inputString.getBytes();
	Todo todler = new Todo(0, retrieveLoggedinUserName(), "", new Date(), false, 81, b);

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

//		PrintWriter out = response.getWriter();
//		out.println("this is doGet on ListTodosServlet");
		request.setAttribute("todos", todler);
		request.getRequestDispatcher("/WEB-INF/views/list-todos.jsp").forward(request, response);

	}

	private String retrieveLoggedinUserName() {
		return "agus";
	}
}
